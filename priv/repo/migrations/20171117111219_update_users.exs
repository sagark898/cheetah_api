defmodule CheetahApi.Repo.Migrations.UpdateUsers do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :relationship_status, :string
      add :wedding_anniversay, :date
      add :about_me, :text

      remove :mobile_number
      remove :current_city
    end
  end
end

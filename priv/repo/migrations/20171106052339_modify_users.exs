defmodule CheetahApi.Repo.Migrations.ModifyUsers do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :email_address, :string
      add :gender, :string
      add :dob, :date
      add :mobile_number, :string
      add :current_city, :string
      add :profile_pic_url, :string
      add :hashed_password, :string
      add :auth_token, :string

      add :role_id, references(:roles)
    end

    create index(:users, [:role_id])
    create unique_index(:users, [:email_address])
  end
end
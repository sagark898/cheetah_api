defmodule CheetahApi.Repo.Migrations.ModifyContactDetails do
  use Ecto.Migration

  def change do
    alter table(:contact_details) do
      add :current_city, :string
      add :home_town, :string
      add :mobile_number, :string
      add :home_phone_number, :string
      add :work_phone_number, :string

    end
  end
end

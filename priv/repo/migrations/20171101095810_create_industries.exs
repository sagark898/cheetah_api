defmodule CheetahApi.Repo.Migrations.CreateIndustries do
  use Ecto.Migration

  def change do
    create table(:industries) do
      add :name, :string

      timestamps()
    end

    create unique_index(:industries, [:name])
  end
end

defmodule CheetahApi.Repo.Migrations.CreateEducationalDetails do
  use Ecto.Migration

  def change do
    create table(:educational_details) do
      add :institution, :string
      add :degree, :string
      add :start_date, :date
      add :end_date, :date
      add :location, :string

      add :user_id, references(:users, on_delete: :delete_all)

      timestamps()
    end

    create index(:educational_details, [:user_id])
  end
end

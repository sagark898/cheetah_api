--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.10
-- Dumped by pg_dump version 9.5.10

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: batches; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE batches (
    id bigint NOT NULL,
    start_date date,
    end_date date,
    stream_id bigint,
    inserted_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: batches_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE batches_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: batches_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE batches_id_seq OWNED BY batches.id;


--
-- Name: companies; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE companies (
    id bigint NOT NULL,
    name character varying(255),
    inserted_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: companies_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE companies_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: companies_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE companies_id_seq OWNED BY companies.id;


--
-- Name: contact_details; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE contact_details (
    id bigint NOT NULL,
    primary_email character varying(255),
    secondary_email character varying(255),
    website_url character varying(255),
    user_id bigint,
    inserted_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    current_city character varying(255),
    home_town character varying(255),
    mobile_number character varying(255),
    home_phone_number character varying(255),
    work_phone_number character varying(255)
);


--
-- Name: contact_details_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE contact_details_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: contact_details_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE contact_details_id_seq OWNED BY contact_details.id;


--
-- Name: correspondence_addresses; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE correspondence_addresses (
    id bigint NOT NULL,
    address character varying(255),
    locality character varying(255),
    city character varying(255),
    postal_code character varying(255),
    contact_detail_id bigint,
    inserted_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: correspondence_addresses_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE correspondence_addresses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: correspondence_addresses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE correspondence_addresses_id_seq OWNED BY correspondence_addresses.id;


--
-- Name: courses; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE courses (
    id bigint NOT NULL,
    name character varying(255),
    duration integer
);


--
-- Name: courses_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE courses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: courses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE courses_id_seq OWNED BY courses.id;


--
-- Name: educational_details; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE educational_details (
    id bigint NOT NULL,
    institution character varying(255),
    degree character varying(255),
    start_date date,
    end_date date,
    location character varying(255),
    user_id bigint,
    inserted_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: educational_details_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE educational_details_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: educational_details_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE educational_details_id_seq OWNED BY educational_details.id;


--
-- Name: industries; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE industries (
    id bigint NOT NULL,
    name character varying(255),
    inserted_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: industries_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE industries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: industries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE industries_id_seq OWNED BY industries.id;


--
-- Name: professional_roles; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE professional_roles (
    id bigint NOT NULL,
    name character varying(255),
    inserted_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: professional_roles_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE professional_roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: professional_roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE professional_roles_id_seq OWNED BY professional_roles.id;


--
-- Name: roles; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE roles (
    id bigint NOT NULL,
    name character varying(255)
);


--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE roles_id_seq OWNED BY roles.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE schema_migrations (
    version bigint NOT NULL,
    inserted_at timestamp without time zone
);


--
-- Name: skills; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE skills (
    id bigint NOT NULL,
    name character varying(255),
    inserted_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: skills_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE skills_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: skills_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE skills_id_seq OWNED BY skills.id;


--
-- Name: social_profiles; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE social_profiles (
    id bigint NOT NULL,
    name character varying(255),
    url character varying(255),
    user_id bigint,
    inserted_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: social_profiles_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE social_profiles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: social_profiles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE social_profiles_id_seq OWNED BY social_profiles.id;


--
-- Name: streams; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE streams (
    id bigint NOT NULL,
    name character varying(255),
    course_id bigint,
    inserted_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: streams_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE streams_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: streams_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE streams_id_seq OWNED BY streams.id;


--
-- Name: user_roles; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE user_roles (
    id bigint NOT NULL,
    user_id bigint,
    role_id bigint,
    inserted_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: user_roles_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE user_roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE user_roles_id_seq OWNED BY user_roles.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE users (
    id bigint NOT NULL,
    inserted_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    email_address character varying(255),
    gender character varying(255),
    dob date,
    profile_pic_url character varying(255),
    hashed_password character varying(255),
    auth_token character varying(255),
    role_id bigint,
    first_name character varying(255),
    last_name character varying(255),
    relationship_status character varying(255),
    wedding_anniversay date,
    about_me text
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: work_details; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE work_details (
    id bigint NOT NULL,
    description text,
    start_date date,
    end_date date,
    location character varying(255),
    work_experience_id bigint,
    professional_role_id bigint,
    company_id bigint,
    inserted_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: work_details_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE work_details_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: work_details_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE work_details_id_seq OWNED BY work_details.id;


--
-- Name: work_experience_industries; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE work_experience_industries (
    id bigint NOT NULL,
    work_experience_id bigint,
    industry_id bigint
);


--
-- Name: work_experience_industries_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE work_experience_industries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: work_experience_industries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE work_experience_industries_id_seq OWNED BY work_experience_industries.id;


--
-- Name: work_experience_professional_roles; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE work_experience_professional_roles (
    id bigint NOT NULL,
    work_experience_id bigint,
    professional_role_id bigint
);


--
-- Name: work_experience_professional_roles_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE work_experience_professional_roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: work_experience_professional_roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE work_experience_professional_roles_id_seq OWNED BY work_experience_professional_roles.id;


--
-- Name: work_experience_skills; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE work_experience_skills (
    id bigint NOT NULL,
    work_experience_id bigint,
    skill_id bigint
);


--
-- Name: work_experience_skills_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE work_experience_skills_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: work_experience_skills_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE work_experience_skills_id_seq OWNED BY work_experience_skills.id;


--
-- Name: work_experiences; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE work_experiences (
    id bigint NOT NULL,
    professional_headline text,
    total_years_experience integer,
    user_id bigint,
    inserted_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: work_experiences_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE work_experiences_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: work_experiences_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE work_experiences_id_seq OWNED BY work_experiences.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY batches ALTER COLUMN id SET DEFAULT nextval('batches_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY companies ALTER COLUMN id SET DEFAULT nextval('companies_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY contact_details ALTER COLUMN id SET DEFAULT nextval('contact_details_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY correspondence_addresses ALTER COLUMN id SET DEFAULT nextval('correspondence_addresses_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY courses ALTER COLUMN id SET DEFAULT nextval('courses_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY educational_details ALTER COLUMN id SET DEFAULT nextval('educational_details_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY industries ALTER COLUMN id SET DEFAULT nextval('industries_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY professional_roles ALTER COLUMN id SET DEFAULT nextval('professional_roles_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY roles ALTER COLUMN id SET DEFAULT nextval('roles_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY skills ALTER COLUMN id SET DEFAULT nextval('skills_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY social_profiles ALTER COLUMN id SET DEFAULT nextval('social_profiles_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY streams ALTER COLUMN id SET DEFAULT nextval('streams_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_roles ALTER COLUMN id SET DEFAULT nextval('user_roles_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY work_details ALTER COLUMN id SET DEFAULT nextval('work_details_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY work_experience_industries ALTER COLUMN id SET DEFAULT nextval('work_experience_industries_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY work_experience_professional_roles ALTER COLUMN id SET DEFAULT nextval('work_experience_professional_roles_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY work_experience_skills ALTER COLUMN id SET DEFAULT nextval('work_experience_skills_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY work_experiences ALTER COLUMN id SET DEFAULT nextval('work_experiences_id_seq'::regclass);


--
-- Name: batches_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY batches
    ADD CONSTRAINT batches_pkey PRIMARY KEY (id);


--
-- Name: companies_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY companies
    ADD CONSTRAINT companies_pkey PRIMARY KEY (id);


--
-- Name: contact_details_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY contact_details
    ADD CONSTRAINT contact_details_pkey PRIMARY KEY (id);


--
-- Name: correspondence_addresses_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY correspondence_addresses
    ADD CONSTRAINT correspondence_addresses_pkey PRIMARY KEY (id);


--
-- Name: courses_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY courses
    ADD CONSTRAINT courses_pkey PRIMARY KEY (id);


--
-- Name: educational_details_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY educational_details
    ADD CONSTRAINT educational_details_pkey PRIMARY KEY (id);


--
-- Name: industries_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY industries
    ADD CONSTRAINT industries_pkey PRIMARY KEY (id);


--
-- Name: professional_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY professional_roles
    ADD CONSTRAINT professional_roles_pkey PRIMARY KEY (id);


--
-- Name: roles_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: skills_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY skills
    ADD CONSTRAINT skills_pkey PRIMARY KEY (id);


--
-- Name: social_profiles_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY social_profiles
    ADD CONSTRAINT social_profiles_pkey PRIMARY KEY (id);


--
-- Name: streams_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY streams
    ADD CONSTRAINT streams_pkey PRIMARY KEY (id);


--
-- Name: user_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_roles
    ADD CONSTRAINT user_roles_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: work_details_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY work_details
    ADD CONSTRAINT work_details_pkey PRIMARY KEY (id);


--
-- Name: work_experience_industries_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY work_experience_industries
    ADD CONSTRAINT work_experience_industries_pkey PRIMARY KEY (id);


--
-- Name: work_experience_professional_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY work_experience_professional_roles
    ADD CONSTRAINT work_experience_professional_roles_pkey PRIMARY KEY (id);


--
-- Name: work_experience_skills_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY work_experience_skills
    ADD CONSTRAINT work_experience_skills_pkey PRIMARY KEY (id);


--
-- Name: work_experiences_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY work_experiences
    ADD CONSTRAINT work_experiences_pkey PRIMARY KEY (id);


--
-- Name: companies_name_index; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX companies_name_index ON companies USING btree (name);


--
-- Name: contact_details_primary_email_index; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX contact_details_primary_email_index ON contact_details USING btree (primary_email);


--
-- Name: contact_details_user_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX contact_details_user_id_index ON contact_details USING btree (user_id);


--
-- Name: correspondence_addresses_contact_detail_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX correspondence_addresses_contact_detail_id_index ON correspondence_addresses USING btree (contact_detail_id);


--
-- Name: educational_details_user_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX educational_details_user_id_index ON educational_details USING btree (user_id);


--
-- Name: industries_name_index; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX industries_name_index ON industries USING btree (name);


--
-- Name: professional_roles_name_index; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX professional_roles_name_index ON professional_roles USING btree (name);


--
-- Name: roles_name_index; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX roles_name_index ON roles USING btree (name);


--
-- Name: skills_name_index; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX skills_name_index ON skills USING btree (name);


--
-- Name: social_profiles_user_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX social_profiles_user_id_index ON social_profiles USING btree (user_id);


--
-- Name: user_roles_role_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX user_roles_role_id_index ON user_roles USING btree (role_id);


--
-- Name: user_roles_user_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX user_roles_user_id_index ON user_roles USING btree (user_id);


--
-- Name: users_email_address_index; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX users_email_address_index ON users USING btree (email_address);


--
-- Name: users_role_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX users_role_id_index ON users USING btree (role_id);


--
-- Name: work_details_work_experience_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX work_details_work_experience_id_index ON work_details USING btree (work_experience_id);


--
-- Name: work_experience_industries_industry_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX work_experience_industries_industry_id_index ON work_experience_industries USING btree (industry_id);


--
-- Name: work_experience_industries_work_experience_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX work_experience_industries_work_experience_id_index ON work_experience_industries USING btree (work_experience_id);


--
-- Name: work_experience_industries_work_experience_id_industry_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX work_experience_industries_work_experience_id_industry_id_index ON work_experience_industries USING btree (work_experience_id, industry_id);


--
-- Name: work_experience_professional_roles_professional_role_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX work_experience_professional_roles_professional_role_id_index ON work_experience_professional_roles USING btree (professional_role_id);


--
-- Name: work_experience_professional_roles_work_experience_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX work_experience_professional_roles_work_experience_id_index ON work_experience_professional_roles USING btree (work_experience_id);


--
-- Name: work_experience_professional_roles_work_experience_id_professio; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX work_experience_professional_roles_work_experience_id_professio ON work_experience_professional_roles USING btree (work_experience_id, professional_role_id);


--
-- Name: work_experience_skills_skill_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX work_experience_skills_skill_id_index ON work_experience_skills USING btree (skill_id);


--
-- Name: work_experience_skills_work_experience_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX work_experience_skills_work_experience_id_index ON work_experience_skills USING btree (work_experience_id);


--
-- Name: work_experience_skills_work_experience_id_skill_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX work_experience_skills_work_experience_id_skill_id_index ON work_experience_skills USING btree (work_experience_id, skill_id);


--
-- Name: batches_stream_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY batches
    ADD CONSTRAINT batches_stream_id_fkey FOREIGN KEY (stream_id) REFERENCES streams(id) ON DELETE CASCADE;


--
-- Name: contact_details_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY contact_details
    ADD CONSTRAINT contact_details_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;


--
-- Name: correspondence_addresses_contact_detail_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY correspondence_addresses
    ADD CONSTRAINT correspondence_addresses_contact_detail_id_fkey FOREIGN KEY (contact_detail_id) REFERENCES contact_details(id) ON DELETE CASCADE;


--
-- Name: educational_details_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY educational_details
    ADD CONSTRAINT educational_details_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;


--
-- Name: social_profiles_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY social_profiles
    ADD CONSTRAINT social_profiles_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;


--
-- Name: streams_course_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY streams
    ADD CONSTRAINT streams_course_id_fkey FOREIGN KEY (course_id) REFERENCES courses(id) ON DELETE CASCADE;


--
-- Name: user_roles_role_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_roles
    ADD CONSTRAINT user_roles_role_id_fkey FOREIGN KEY (role_id) REFERENCES roles(id);


--
-- Name: user_roles_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_roles
    ADD CONSTRAINT user_roles_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;


--
-- Name: users_role_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_role_id_fkey FOREIGN KEY (role_id) REFERENCES roles(id);


--
-- Name: work_details_company_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY work_details
    ADD CONSTRAINT work_details_company_id_fkey FOREIGN KEY (company_id) REFERENCES companies(id);


--
-- Name: work_details_professional_role_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY work_details
    ADD CONSTRAINT work_details_professional_role_id_fkey FOREIGN KEY (professional_role_id) REFERENCES professional_roles(id);


--
-- Name: work_details_work_experience_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY work_details
    ADD CONSTRAINT work_details_work_experience_id_fkey FOREIGN KEY (work_experience_id) REFERENCES work_experiences(id) ON DELETE CASCADE;


--
-- Name: work_experience_industries_industry_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY work_experience_industries
    ADD CONSTRAINT work_experience_industries_industry_id_fkey FOREIGN KEY (industry_id) REFERENCES industries(id);


--
-- Name: work_experience_industries_work_experience_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY work_experience_industries
    ADD CONSTRAINT work_experience_industries_work_experience_id_fkey FOREIGN KEY (work_experience_id) REFERENCES work_experiences(id) ON DELETE CASCADE;


--
-- Name: work_experience_professional_roles_professional_role_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY work_experience_professional_roles
    ADD CONSTRAINT work_experience_professional_roles_professional_role_id_fkey FOREIGN KEY (professional_role_id) REFERENCES professional_roles(id);


--
-- Name: work_experience_professional_roles_work_experience_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY work_experience_professional_roles
    ADD CONSTRAINT work_experience_professional_roles_work_experience_id_fkey FOREIGN KEY (work_experience_id) REFERENCES work_experiences(id) ON DELETE CASCADE;


--
-- Name: work_experience_skills_skill_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY work_experience_skills
    ADD CONSTRAINT work_experience_skills_skill_id_fkey FOREIGN KEY (skill_id) REFERENCES skills(id);


--
-- Name: work_experience_skills_work_experience_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY work_experience_skills
    ADD CONSTRAINT work_experience_skills_work_experience_id_fkey FOREIGN KEY (work_experience_id) REFERENCES work_experiences(id) ON DELETE CASCADE;


--
-- Name: work_experiences_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY work_experiences
    ADD CONSTRAINT work_experiences_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

INSERT INTO "schema_migrations" (version) VALUES (20171101053810), (20171101083042), (20171101095724), (20171101095750), (20171101095758), (20171101095804), (20171101095810), (20171101095843), (20171101101831), (20171101101841), (20171101101850), (20171101112224), (20171102111317), (20171102114703), (20171102115303), (20171106051600), (20171106052339), (20171106060903), (20171106060944), (20171106060952), (20171110103610), (20171117111219), (20171117111757);


defmodule CheetahApi.ProfessionalDetails.Company do
  use CheetahApi.Data

  schema "companies" do
    field :name, :string

    timestamps()
  end

  def changeset(struct, params \\ %{}) do
    struct
      |> cast(params, [:name])
      |> validate_required([:name])
  end

end
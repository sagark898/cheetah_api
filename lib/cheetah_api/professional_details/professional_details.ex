defmodule CheetahApi.ProfessionalDetails do
  use CheetahApi.Data

  alias CheetahApi.ProfessionalDetails.{WorkExperience, WorkDetail, EducationalDetail}

  @doc """
  Creates a Work Experience.
  ## Examples
      params = %{total_years_experience: 5, user_id: 1, professional_role_ids: [1,2], industry_ids: [1,2], skill_ids: [1,2], work_details: %{0 => %{professional_role_id: 1, company_id: 1}}}
      iex> create_work_experience(params)
      {:ok, %WorkExperience{}}
  """
  def create_work_experience(params) do
    %WorkExperience{}
    |> WorkExperience.changeset(params)
    |> Repo.insert
  end

  def get_work_experience(%{"id" => id}) do
    Repo.get(WorkExperience, id)
  end

  def update_work_experience(struct, params) do
    struct
    |> WorkExperience.changeset(params)
    |> Repo.update
  end

  def get_work_detail(%{"id" => id}) do
    Repo.get(WorkDetail, id)
  end

  @doc """
  Creates a Work Detail.
  ## Examples
      params = %{professional_role_id: 1, company_id: 1, work_experience_id: 1}
      iex> create_work_detail(params)
      {:ok, %WorkDetail{}}
  """
  def create_work_detail(params) do
    %WorkDetail{}
    |> WorkDetail.changeset(params)
    |> Repo.insert
  end

  def update_work_detail(struct, params) do
    struct
    |> WorkDetail.changeset(params)
    |> Repo.update
  end

  def delete_work_detail(struct) do
    Repo.delete(struct)
  end

  def get_educational_detail(%{"id" => id}) do
    Repo.get(EducationalDetail, id)
  end

  def create_educational_detail(params) do
    %EducationalDetail{}
    |> EducationalDetail.changeset(params)
    |> Repo.insert
  end
  
  def update_educational_detail(struct, params) do
    struct
    |> EducationalDetail.changeset(params)
    |> Repo.update
  end

  def delete_educational_detail(struct) do
    Repo.delete(struct)
  end

end
defmodule CheetahApi.ProfessionalDetails.ProfessionalRole do
  use CheetahApi.Data

  schema "professional_roles" do
    field :name, :string

    timestamps()
  end

  def changeset(struct, params \\ %{}) do
    struct
      |> cast(params, [:name])
      |> validate_required([:name])
  end

end
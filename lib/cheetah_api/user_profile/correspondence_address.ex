defmodule CheetahApi.UserProfile.CorrespondenceAddress do
  use CheetahApi.Data

  alias CheetahApi.UserProfile.ContactDetail

  schema "correspondence_addresses" do
    field :address, :string
    field :locality, :string
    field :city, :string
    field :postal_code, :string

    belongs_to :contact_detail, ContactDetail

    timestamps()
  end

  @required_fields ~w(address locality city postal_code)a

  def changeset(struct, params\\ %{}) do
    struct
    |> cast(params, @required_fields)
    |> validate_required(@required_fields)
  end

end
defmodule CheetahApi.UserProfile.ContactDetail do
  use CheetahApi.Data

  alias CheetahApi.UserProfile.{User, CorrespondenceAddress}

  schema "contact_details" do
    field :primary_email, :string
    field :secondary_email, :string
    field :website_url, :string
    field :current_city, :string
    field :home_town, :string
    field :mobile_number, :string
    field :home_phone_number, :string
    field :work_phone_number, :string

    has_one :correspondence_address, CorrespondenceAddress

    belongs_to :user, User

    timestamps()
  end

  @required_fields ~w(primary_email current_city mobile_number)a
  @optional_fields ~w(secondary_email website_url home_town home_phone_number work_phone_number)a

  def changeset(struct, params\\ %{}) do
    struct
    |> cast(params, @required_fields ++ @optional_fields)
    |> validate_required(@required_fields)
    |> validate_format(:primary_email, ~r/@/)
    |> validate_format(:secondary_email, ~r/@/)
    |> validate_format(:website_url, valid_format())
    |> cast_assoc(:correspondence_address)
  end

  defp valid_format do
    ~r/\A((http|https):\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,}(([0-9]{1,5})?\/.*)?#=\z/ix
  end

end
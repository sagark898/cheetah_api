defmodule CheetahApi.UserProfile.User do
  use CheetahApi.Data

  alias CheetahApi.UserProfile.{ContactDetail, SocialProfile}
  alias CheetahApi.ProfessionalDetails.{WorkExperience, EducationalDetail}

  schema "users" do
    field :email_address, :string
    field :gender, :string
    field :dob, :date
    field :profile_pic_url, :string
    field :first_name, :string
    field :last_name, :string

    has_one :contact_detail, ContactDetail
    has_one :work_experience, WorkExperience
    has_one :educational_detail, EducationalDetail

    has_many :social_profiles, SocialProfile

    timestamps()
  end

  @required_fields ~w(first_name last_name email_address gender dob)a

  def changeset(struct, params\\ %{}) do
    struct
      |> cast(params, @required_fields)
      |> validate_required(@required_fields)
      |> validate_format(:email_address, ~r/@/)
  end

end

defmodule CheetahApi.UserProfile do
  use CheetahApi.Data

  alias CheetahApi.UserProfile.{User, ContactDetail}

  def get(%{"id" => id}) do
    Repo.get(User, id)
  end

  def update(user, params) do
    user
    |> User.changeset(params)
    |> Repo.update
  end

  def get_contact_detail(%{"id" => id}) do
    Repo.get(ContactDetail, id)
  end

  def update_contact_detail(contact_detail, params) do
    contact_detail
    |> ContactDetail.changeset(params)
    |> Repo.update
  end

end
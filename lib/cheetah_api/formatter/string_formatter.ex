defmodule CheetahApi.StringFormatter do
  def camelize(key), do: key |> Recase.to_camel
end
defmodule CheetahApi.Account do
  import Comeonin.Bcrypt, only: [checkpw: 2, dummy_checkpw: 0]
  import Plug.Conn
  use CheetahApi.Data

  alias CheetahApi.Account.{User}

  def create(params\\ %{}) do
    %User{}
    |> User.email_changeset(params)
    |> Repo.insert
  end

  def update(params, user) do
    user
    |> User.registration_changeset(params)
    |> Repo.update
  end

  def find(%{"email_address" => email}) do
    User
    |> Repo.get_by(email_address: email)
  end

  def login_by_email_and_pass(conn, %{"password" => password} = params) do
    with %User{} = user <- find(params),
          true <- checkpw(password, user.hashed_password)
    do
      {:ok, login(conn, user)}
    else
      nil -> dummy_checkpw()
             nil
      false -> {:error, :not_authorized}
    end
  end

  defp login(conn, user) do
    jwt = conn |> generate_token(user)
    conn
    |> assign(:current_user, user)
    |> put_resp_header("authorization", "Bearer #{jwt}")
  end

  def generate_token(conn, user) do
    Guardian.Plug.api_sign_in(conn, user) |> Guardian.Plug.current_token
  end

end

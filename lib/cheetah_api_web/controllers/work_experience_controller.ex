defmodule CheetahApiWeb.WorkExperienceController do
  use CheetahApiWeb, :controller

  alias CheetahApi.ProfessionalDetails.{WorkExperience}
  alias CheetahApi.ProfessionalDetails
  alias CheetahApi.Repo

  @preload [:work_details, :professional_roles, :industries, :skills]

  action_fallback CheetahApiWeb.FallbackController

  def show(conn, params) do
    with %WorkExperience{} = work_experience <- ProfessionalDetails.get_work_experience(params),
         %WorkExperience{} = work_experience <- Repo.preload(work_experience, @preload)
    do
      conn |> render("show.json-api", data: work_experience)
    end
  end

  def update(conn, params) do
    with %WorkExperience{} = work_experience <- ProfessionalDetails.get_work_experience(params),
         %WorkExperience{} = work_experience <- Repo.preload(work_experience, @preload),
         {:ok, %WorkExperience{} = work_experience} <- ProfessionalDetails.update_work_experience(work_experience, params)
    do
      conn |> render("show.json-api", data: work_experience)
    end
  end

end
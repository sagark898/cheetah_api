defmodule CheetahApiWeb.UserController do
  use CheetahApiWeb, :controller

  alias CheetahApi.UserProfile.User
  alias CheetahApi.UserProfile
  alias CheetahApi.Repo

  action_fallback CheetahApiWeb.FallbackController

  def show(conn, params) do
    with %User{} = user <- UserProfile.get(params),
         %User{} = user <- preload(user)
    do
      conn |> render("show.json-api", data: user)
    end
  end

  def update(conn, params) do
    with %User{} = user <- UserProfile.get(params),
         {:ok, user} <- user |> UserProfile.update(params),
         %User{} = user <- preload(user)
    do
      conn |> render("show.json-api", data: user)
    end
  end

  @preloads [
    [contact_detail: :correspondence_address], 
    [work_experience: [:work_details, :professional_roles, :industries, :skills]],
    :educational_detail
  ]

  def preload(data) do
    Repo.preload(data, @preloads)
  end

end
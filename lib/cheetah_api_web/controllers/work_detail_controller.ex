defmodule CheetahApiWeb.WorkDetailController do
  use CheetahApiWeb, :controller

  alias CheetahApi.ProfessionalDetails
  alias CheetahApi.ProfessionalDetails.WorkDetail

  action_fallback CheetahApiWeb.FallbackController

  def show(conn, params) do
    with %WorkDetail{} = work_detail <- ProfessionalDetails.get_work_detail(params)
    do
      conn |> render("show.json-api", data: work_detail)
    end
  end

  def create(conn, params) do
    with {:ok, %WorkDetail{} = work_detail} <- ProfessionalDetails.create_work_detail(params)
    do
      conn |> render("show.json-api", data: work_detail)
    end
  end

  def update(conn, params) do
    with %WorkDetail{} = work_detail <- ProfessionalDetails.get_work_detail(params),
         {:ok, %WorkDetail{} = work_detail} <- work_detail |> ProfessionalDetails.update_work_detail(params)
    do
      conn |> render("show.json-api", data: work_detail)
    end
  end

  def delete(conn, params) do
    with %WorkDetail{} = work_detail <- ProfessionalDetails.get_work_detail(params),
         {:ok, work_detail} <- work_detail |> ProfessionalDetails.delete_work_detail
    do
      conn |> render("show.json-api", data: work_detail)
    end
  end

end

defmodule CheetahApiWeb.AccountController do
  use CheetahApiWeb, :controller

  alias CheetahApi.Account
  alias CheetahApi.Account.User

  action_fallback CheetahApiWeb.FallbackController

  def register_email(conn, params) do
    with {:ok , %User{} = user} <- Account.create(params),
         jwt <- Account.generate_token(conn, user)
      do
        conn
        |> put_status(:created)
        |> put_resp_header("authorization", "Bearer #{jwt}")
        |> render("show.json-api", data: user)
      end
  end

  def create_account(conn, params) do
    with %User{} = user <- params |> Account.find,
         {:ok, user} <- params |> Account.update(user)
    do
      conn |> render("show.json-api", data: user)
    end
  end

  def sign_in(conn, params) do
    with {:ok, conn} <- conn |> Account.login_by_email_and_pass(params),
         {:ok, user} = Map.fetch(conn.assigns, :current_user)
    do
      conn |> render("show.json-api", data: user)
    end
  end

end

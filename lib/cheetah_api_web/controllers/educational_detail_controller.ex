defmodule CheetahApiWeb.EducationalDetailController do
  use CheetahApiWeb, :controller

  alias CheetahApi.ProfessionalDetails
  alias CheetahApi.ProfessionalDetails.EducationalDetail

  action_fallback CheetahApiWeb.FallbackController

  def show(conn, params) do
    with %EducationalDetail{} = educational_detail <- ProfessionalDetails.get_educational_detail(params)
    do
      conn |> render("show.json-api", data: educational_detail)
    end
  end

  def create(conn, params) do
    with {:ok, %EducationalDetail{} = educational_detail} <- ProfessionalDetails.create_educational_detail(params)
    do
      conn |> render("show.json-api", data: educational_detail)
    end 
  end

  def update(conn, params) do
    with %EducationalDetail{} = educational_detail <- ProfessionalDetails.get_educational_detail(params),
         {:ok, %EducationalDetail{} = educational_detail} <- educational_detail |> ProfessionalDetails.update_educational_detail(params)
    do
      conn |> render("show.json-api", data: educational_detail)
    end 
  end

  def delete(conn, params) do
    with %EducationalDetail{} = educational_detail <- ProfessionalDetails.get_educational_detail(params),
         {:ok, %EducationalDetail{} = educational_detail} <- educational_detail |> ProfessionalDetails.delete_educational_detail
    do
      conn |> render("show.json-api", data: educational_detail)
    end
  end

end
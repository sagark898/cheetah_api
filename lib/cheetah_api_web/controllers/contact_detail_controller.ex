defmodule CheetahApiWeb.ContactDetailController do
  use CheetahApiWeb, :controller

  alias CheetahApi.UserProfile.ContactDetail
  alias CheetahApi.UserProfile
  alias CheetahApi.Repo

  action_fallback CheetahApiWeb.FallbackController

  def show(conn, params) do
    with %ContactDetail{} = contact_detail <- UserProfile.get_contact_detail(params),
         %ContactDetail{} = contact_detail <- Repo.preload(contact_detail, :correspondence_address)
    do
      conn |> render("show.json-api", data: contact_detail)
    end
  end

  def update(conn, params) do
    with %ContactDetail{} = contact_detail <- UserProfile.get_contact_detail(params),
         %ContactDetail{} = contact_detail <- Repo.preload(contact_detail, :correspondence_address),
         {:ok, contact_detail} <- UserProfile.update_contact_detail(contact_detail, params)
    do
      conn |> render("show.json-api", data: contact_detail)
    end
  end

end
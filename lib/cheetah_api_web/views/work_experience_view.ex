defmodule CheetahApiWeb.WorkExperienceView do
  use CheetahApiWeb, :view
  use JaSerializer.PhoenixView

  attributes [
    :professional_headline, :total_years_experience
  ]

  has_many :work_details, include: true, serializer: CheetahApiWeb.WorkDetailView
  has_many :professional_roles, include: true, serializer: CheetahApiWeb.ProfessionalRoleView
  has_many :industries, include: true, serializer: CheetahApiWeb.IndustryView
  has_many :skills, include: true, serializer: CheetahApiWeb.SkillView

end
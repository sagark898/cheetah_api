defmodule CheetahApiWeb.EducationalDetailView do
  use CheetahApiWeb, :view
  use JaSerializer.PhoenixView

  attributes [
    :institution, :degree, :start_date, :end_date, :location
  ]

end
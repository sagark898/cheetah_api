defmodule CheetahApiWeb.ContactDetailView do
  use CheetahApiWeb, :view
  use JaSerializer.PhoenixView

  attributes [
    :primary_email, :secondary_email, :website_url, :current_city,
    :home_town, :mobile_number, :home_phone_number, :work_phone_number
  ]

  has_one :correspondence_address, include: true, serializer: CheetahApiWeb.CorrespondenceAddressView

end
defmodule CheetahApiWeb.UserView do
  use CheetahApiWeb, :view
  use JaSerializer.PhoenixView

  attributes [
    :first_name, :last_name, :email_address, :gender, 
    :dob, :profile_pic_url
  ]

  has_one :contact_detail, include: true, serializer: CheetahApiWeb.ContactDetailView
  has_one :work_experience, include: true, serializer: CheetahApiWeb.WorkExperienceView
  has_one :educational_detail, serializer: CheetahApiWeb.EducationalDetailView
  
end

defmodule CheetahApiWeb.CorrespondenceAddressView do
  use CheetahApiWeb, :view
  use JaSerializer.PhoenixView

  attributes [
    :address, :locality, :city, :postal_code
  ]

end
defmodule CheetahApiWeb.IndustryView do
  use CheetahApiWeb, :view
  use JaSerializer.PhoenixView

  attributes [
    :name
  ]
end
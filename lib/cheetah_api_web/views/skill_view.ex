defmodule CheetahApiWeb.SkillView do
  use CheetahApiWeb, :view
  use JaSerializer.PhoenixView

  attributes [
    :name
  ]
end
defmodule CheetahApi.ProfessionalDetails.CompanyTest do
  use CheetahApi.DataCase

  alias CheetahApi.ProfessionalDetails.Company

  @valid_attrs %{
    name: "facebook"
  }
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Company.changeset(%Company{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Company.changeset(%Company{}, @invalid_attrs)
    refute changeset.valid?
  end

end
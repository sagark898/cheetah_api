defmodule CheetahApi.ProfessionalDetails.ProfessionalRoleTest do
  use CheetahApi.DataCase

  alias CheetahApi.ProfessionalDetails.ProfessionalRole

  @valid_attrs %{
    name: "manager"
  }
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = ProfessionalRole.changeset(%ProfessionalRole{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = ProfessionalRole.changeset(%ProfessionalRole{}, @invalid_attrs)
    refute changeset.valid?
  end

end

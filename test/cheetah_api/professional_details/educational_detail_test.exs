defmodule CheetahApi.ProfessionalDetails.EducationalDetailTest do
  use CheetahApi.DataCase

  alias CheetahApi.ProfessionalDetails.EducationalDetail

  @valid_attrs %{
    institution: "ait pune", degree: "BE", start_date: "2016-04-25", location: "pune"
  }
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = EducationalDetail.changeset(%EducationalDetail{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = EducationalDetail.changeset(%EducationalDetail{}, @invalid_attrs)
    refute changeset.valid?
  end

end
defmodule CheetahApi.ProfessionalDetails.WorkDetailTest do
  use CheetahApi.DataCase

  alias CheetahApi.ProfessionalDetails.WorkDetail

  @valid_attrs %{
    start_date: "2016-04-25", professional_role_id: "1", company_id: "1"
  }
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = WorkDetail.changeset(%WorkDetail{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = WorkDetail.changeset(%WorkDetail{}, @invalid_attrs)
    refute changeset.valid?
  end

end
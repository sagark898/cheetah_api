defmodule CheetahApi.ProfessionalDetails.IndustryTest do
  use CheetahApi.DataCase

  alias CheetahApi.ProfessionalDetails.Industry

  @valid_attrs %{
    name: "software"
  }
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Industry.changeset(%Industry{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Industry.changeset(%Industry{}, @invalid_attrs)
    refute changeset.valid?
  end

end
defmodule CheetahApi.ProfessionalDetails.WorkExperienceTest do
  use CheetahApi.DataCase

  alias CheetahApi.ProfessionalDetails.WorkExperience

  @valid_attrs %{
    total_years_experience: "5", user_id: "1"
  }
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = WorkExperience.changeset(%WorkExperience{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = WorkExperience.changeset(%WorkExperience{}, @invalid_attrs)
    refute changeset.valid?
  end

end
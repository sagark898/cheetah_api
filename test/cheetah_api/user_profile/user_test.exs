defmodule CheetahApi.UserProfile.UserTest do
  use CheetahApi.DataCase

  alias CheetahApi.UserProfile.User

  @valid_attrs %{
    first_name: "chandra", last_name: "shekhar",
    email_address: "shekhar@gmail.com", gender: "male",
    dob: "1993-04-25"
  }
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = User.changeset(%User{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = User.changeset(%User{}, @invalid_attrs)
    refute changeset.valid?
  end

  test "email address must contain a character @" do
    attrs = %{@valid_attrs | email_address: "shekhargmail.com"}
    changeset = User.changeset(%User{}, attrs)
    assert %{email_address: ["has invalid format"]} = errors_on(changeset)
  end

end
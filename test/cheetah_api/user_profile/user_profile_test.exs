defmodule CheetahApi.UserProfile.UserProfileTest do
  use CheetahApi.DataCase

  alias CheetahApi.UserProfile
  alias CheetahApi.UserProfile.{User, ContactDetail}

  describe "users" do
    
    @create_attrs %{
      first_name: "chandra", last_name: "shekhar",
      email_address: "shekhar@gmail.com", gender: "male",
      dob: "1993-04-25"
    }
    @update_attrs %{
      first_name: "shekhar", last_name: "chandra",
      email_address: "chandra@gmail.com"
    }

    def user_fixture() do
      {:ok, user} =
        %User{}
        |> User.changeset(@create_attrs)
        |> CheetahApi.Repo.insert

      user
    end

    test "get/1 returns the user with given id" do
      user = user_fixture()
      assert UserProfile.get(%{"id" => user.id}) == user
      assert %User{} = user
    end

    test "update/2 with valid data updates the user" do
      user = user_fixture()
      assert {:ok, user} = UserProfile.update(user, @update_attrs)
      assert %User{first_name: "shekhar", last_name: "chandra", email_address: "chandra@gmail.com"}
             = user
    end

  end

  describe "contact details" do
    @create_attrs %{
      primary_email: "shekhar@gmail.com", secondary_email: "chandra@gmail.com",
      current_city: "pune", mobile_number: "0123456789", home_town: "pune",
      website_url: "www.facebook.com", home_phone_number: "123456789",
      work_phone_number: "987654321"
    }
    @update_attrs %{
      primary_email: "chandra@gmail.com", secondary_email: "shekhar@gmail.com",
      home_town: "jhansi", current_city: "jhansi", mobile_number: "9876543210"
    }

    def contact_detail_fixture() do
      {:ok, contact_detail} =
        %ContactDetail{}
        |> ContactDetail.changeset(@create_attrs)
        |> CheetahApi.Repo.insert

      contact_detail
    end

    test "get/1 returns the contact detail with given id" do
      contact_detail = contact_detail_fixture()
      assert UserProfile.get_contact_detail(%{"id" => contact_detail.id}) == contact_detail
      assert %ContactDetail{} = contact_detail
    end

    test "update/2 with valid data updates the contact detail" do
      contact_detail = contact_detail_fixture()
      assert {:ok, contact_detail} = UserProfile.update_contact_detail(contact_detail, @update_attrs)
      assert %ContactDetail{primary_email: "chandra@gmail.com", secondary_email: "shekhar@gmail.com",
             home_town: "jhansi", current_city: "jhansi", mobile_number: "9876543210"} 
             = contact_detail
    end

  end

end
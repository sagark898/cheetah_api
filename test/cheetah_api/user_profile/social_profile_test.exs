defmodule CheetahApi.UserProfile.SocialProfileTest do
  use CheetahApi.DataCase

  alias CheetahApi.UserProfile.SocialProfile

  @valid_attrs %{
    name: "Google", url: "www.google.com"
  }
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = SocialProfile.changeset(%SocialProfile{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = SocialProfile.changeset(%SocialProfile{}, @invalid_attrs)
    refute changeset.valid?
  end

end
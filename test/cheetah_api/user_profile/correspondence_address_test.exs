defmodule CheetahApi.UserProfile.CorrespondenceAddressTest do
  use CheetahApi.DataCase

  alias CheetahApi.UserProfile.CorrespondenceAddress

  @valid_attrs %{
    address: "111-345, xyz towers", locality: "near abc mall", city: "pune", postal_code: "123456"
  }
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = CorrespondenceAddress.changeset(%CorrespondenceAddress{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = CorrespondenceAddress.changeset(%CorrespondenceAddress{}, @invalid_attrs)
    refute changeset.valid?
  end

end
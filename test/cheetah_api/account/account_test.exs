defmodule CheetahApi.AccountTest do
  use CheetahApiWeb.ConnCase

  alias CheetahApi.Account
  alias CheetahApi.Account.User

  @test_email "test@gmail.com"
  @invalid_email "test"
  @update_params %{
    "email_address" => "test@gmail.com",
    "gender" => "male",
    "dob" => "2015-10-29",
    "password" => "kunal123",
    "first_name" => "Kunal",
    "last_name" => "Powar"
  }
  @invalid_update_params %{
    "email_address" => "test@gmail.com",
    "gender" => "male",
    "first_name" => "Kunal",
    "last_name" => "Powar"
  }

  test "create account for valid user" do
    {:ok, user } = Account.create(%{"email_address" => @test_email})

    assert user.email_address == @test_email
  end

  test "create account with duplicate email" do
    {:ok, _ } = Account.create(%{"email_address" => @test_email})
    {:error, changeset } = Account.create(%{"email_address" => @test_email})
    {error_msg, []} = Keyword.get(changeset.errors, :email_address)

    assert error_msg == "has already been taken"
  end

  test "create account with invalid email" do
    {:error, changeset } = Account.create(%{"email_address" => @invalid_email})
    {error_msg, _} = Keyword.get(changeset.errors, :email_address)

    assert error_msg == "has invalid format"
  end


  test "update user with valid params" do
    {:ok, user } = Account.create(%{"email_address" => @test_email})
    {:ok, %User{} = user} = Account.update(@update_params, user)

    assert %User{}  = user
    assert user.email_address == "test@gmail.com"
    assert user.first_name == "Kunal"
    assert user.last_name == "Powar"
    assert user.password == "kunal123"
    assert user.gender == "male"
  end

  test "update user with missing parameters" do
    {:ok, user } = Account.create(%{"email_address" => @test_email})

    assert {:error, %Ecto.Changeset{} } = Account.update(@invalid_update_params, user)
  end

  test "find user by email_address" do
    {:ok, _ } = Account.create(%{"email_address" => @test_email})
    assert %User{} = Account.find(%{"email_address" => "test@gmail.com"})
  end

  test "login in with valid params", %{conn: conn} do
    {:ok, user } = Account.create(%{"email_address" => @test_email})
    {:ok, %User{}} = Account.update(@update_params, user)

    assert {:ok, %Plug.Conn{}} = Account.login_by_email_and_pass(conn, %{"email_address" => @test_email, "password" => "kunal123"})
  end

  test "login in with invalid email", %{conn: conn} do
    {:ok, user } = Account.create(%{"email_address" => @test_email})
    {:ok, %User{}} = Account.update(@update_params, user)

    refute Account.login_by_email_and_pass(conn, %{"email_address" => @invalid_email, "password" => "kunal123"})
  end

  test "login in with wronf password", %{conn: conn} do
    {:ok, user } = Account.create(%{"email_address" => @test_email})
    {:ok, %User{}} = Account.update(@update_params, user)

    assert {:error, :not_authorized} = Account.login_by_email_and_pass(conn, %{"email_address" => @test_email, "password" => "kunal1"})
  end

end

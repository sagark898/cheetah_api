defmodule UserTest do
  use CheetahApi.DataCase
  alias CheetahApi.Account.User

  @valid_email "philip_white@gmail.com"
  @invalid_email "philipgmail.com"

  @valid_registration %{
    "email_address" => "philip_white@gmail.com",
    "current_city" => "pune",
    "gender" => "male",
    "dob" => "2015-10-29T20:12:30Z",
    "mobile_number" => "1234567890",
    "password" => "philip@1234",
    "first_name" => "Philip",
    "last_name" => "White"
  }
  @invalid_registration %{
    "email_address" => "philip_white@gmail.com",
    "dob" => "2015-10-29T20:12:30Z",
    "mobile_number" => "1234567890",
    "password" => "philip@1234",
    "first_name" => "Philip",
    "last_name" => "White"
  }

  test "create changeset with valid attr" do
    changeset = User.email_changeset(%User{}, %{email_address: @valid_email})

    assert changeset.valid?
  end

  test "create changser with invalid attr" do
    changeset = User.email_changeset(%User{}, %{})

    refute changeset.valid?
  end

  test "create changeset with invalid email" do
    changeset = User.email_changeset(%User{}, %{email_address: @invalid_email})

    refute changeset.valid?
  end

  test "registration changeset with valid attr" do
    changeset = User.registration_changeset(%User{}, @valid_registration)

    assert changeset.valid?
  end

  test "registration changeset with invalid attr" do
    changeset = User.registration_changeset(%User{}, @invalid_registration)

    refute changeset.valid?
  end

end

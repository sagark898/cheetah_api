defmodule CheetahApi.Factory do
  use ExMachina.Ecto, repo: CheetahApi.Repo

  alias CheetahApi.UserProfile.{User}
  alias CheetahApi.ProfessionalDetails.{Company, ProfessionalRole, Industry, Skill, WorkExperience, WorkDetail, EducationalDetail}

  def user_factory do
    %User{
      first_name: sequence(:first_name, &"First#{&1}"),
      last_name: sequence(:last_name, &"Last#{&1}"),
      email_address: sequence(:email_address, &"email-#{&1}@mail.com")
    }
  end

  def company_factory do
    %Company{
      name: sequence("company")
    }
  end

  def professional_role_factory do
    %ProfessionalRole{
      name: sequence("professional_role")
    }
  end

  def industry_factory do
    %Industry{
      name: sequence("industry")
    }
  end

  def skill_factory do
    %Skill{
      name: sequence("skill")
    }
  end

  def work_experience_factory do
    %WorkExperience{
      total_years_experience: "5",
      user: build(:user)
    }
  end

  def work_detail_factory do
    %WorkDetail{
      start_date: ~D[2016-04-25],
      description: sequence("description"),
      location: sequence("location"),
      work_experience: build(:work_experience),
      professional_role: build(:professional_role),
      company: build(:company)
    }
  end

  def educational_detail_factory do
    %EducationalDetail{
      institution: sequence("institution"),
      degree: "BE",
      start_date: ~D[2016-04-25],
      location: sequence("location"),
      user: build(:user)
    }
  end

end
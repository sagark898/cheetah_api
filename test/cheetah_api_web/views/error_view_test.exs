defmodule CheetahApiWeb.ErrorViewTest do
  use CheetahApiWeb.ConnCase, async: true

  # Bring render/3 and render_to_string/3 for testing custom views
  import Phoenix.View

  test "renders 404.json" do
    assert render(CheetahApiWeb.ErrorView, "404.json", []) ==
           %{errors: %{detail: "Not found", status: 404}}
  end

  test "render 500.json" do
    assert render(CheetahApiWeb.ErrorView, "500.json", []) ==
           %{errors: %{detail: "Internal server error", status: 500}}
  end

  test "render any other" do
    assert render(CheetahApiWeb.ErrorView, "505.json", []) ==
           %{errors: %{detail: "Internal server error", status: 500}}
  end

end

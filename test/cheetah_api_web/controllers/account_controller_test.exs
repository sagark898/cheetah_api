defmodule AccountControllerTest do
  use CheetahApiWeb.ConnCase

  alias CheetahApi.Account
  alias CheetahApi.Account.User

  @test_email "philip_white@gmail.com"
  @update_params %{
    "email_address" => "philip_white@gmail.com",
    "current_city" => "pune",
    "gender" => "male",
    "dob" => "2015-10-29T20:12:30Z",
    "mobile_number" => "1234567890",
    "password" => "philip@1234",
    "first_name" => "Philip",
    "last_name" => "White"
  }
  @invalid_update_params %{
    "email_address" => "philip_white@gmail.com",
    "current_city" => "pune",
    "mobile_number" => "1234567890",
    "password" => "philip@1234",
    "first_name" => "Philip",
    "last_name" => "White"
  }

  describe "register_by_email/2" do
      test "register user by email", %{conn: conn} do
        attrs = %{"email_address" => "philip_white@gmail.com"}
        conn = post conn, account_path(conn, :register_email, attrs)

        assert conn.status == 201
      end

      test "register with invalid email", %{conn: conn} do
        attrs = %{"email_address" => "testgmail.com"}
        conn = post conn, account_path(conn, :register_email, attrs)
        response = json_response(conn, 422)

        with %{"errors" => %{"email_address" => error}} <- response
        do
          assert error == "has invalid format"
        end
      end

      test "send empty json", %{conn: conn} do
        attrs = %{}
        conn = post conn, account_path(conn, :register_email, attrs)
        response = json_response(conn, 422)

        with %{"errors" => %{"email_address" => error}} <- response
        do
          assert error == "can't be blank"
        end
      end
  end

  describe "signin" do
    test "sign in with valid attributes", %{conn: conn} do
      attrs = %{"email_address" => @test_email, "password" => "philip@1234"}
      {:ok, %User{} = _user} = create_test_user()

      conn = post conn, account_path(conn, :sign_in, attrs)
      response = json_response(conn, 200)

      with %{"data" => %{ "attributes" => %{
          "emailAddress" => email
        }}} <- response do
          assert email == @test_email
      end
    end

    test "sign in with invalid password", %{conn: conn} do
      attrs = %{"email_address" => "philip_white@gmail.com", "password" => "kunal12"}
      expected_response = %{"errors" => %{"detail" => "Not Authorized", "status" => 401}}
      {:ok, %User{} = _user} = create_test_user()


      conn = post conn, account_path(conn, :sign_in, attrs)
      response = json_response(conn, 401)
      assert expected_response == response
    end

    test "sign in with invalid email", %{conn: conn} do
      attrs = %{"email_address" => "invalid@gmail.com", "password" => "philip@1234"}
      expected_response = %{"errors" => %{"detail" => "Not found", "status" => 404}}
      {:ok, %User{} = _user} = create_test_user()


      conn = post conn, account_path(conn, :sign_in, attrs)
      response = json_response(conn, 404)
      assert expected_response == response
    end
  end

  describe "update user details" do
    
    test "update user with valid arguments", %{conn: conn} do
      {:ok, _user } = Account.create(%{"email_address" => @test_email})

      conn = post conn, account_path(conn, :create_account, @update_params)
      response = json_response(conn, 200)

      with %{"data" => %{"attributes" => %{
          "emailAddress" => email, "firstName" => firstName,
          "lastName" => lastName
        }}} <- response do
          assert email == @test_email
          assert firstName == "Philip"
          assert lastName == "White"
      end
    end

    test "update user with any invalid param", %{conn: conn} do
      {:ok, _user } = Account.create(%{"email_address" => @test_email})

      conn = post conn, account_path(conn, :create_account, @invalid_update_params)
      response = json_response(conn, 422)

      with %{"errors" => %{
          "gender" => gender,
          "dob" => dob
        }} <- response  do
          assert gender == "can't be blank"
          assert dob == "can't be blank"
      end
    end

    test "update user with invalid email", %{conn: conn} do
      {:ok, _user } = Account.create(%{"email_address" => @test_email})

      conn = post conn, account_path(conn, :create_account, %{"email_address" => "test@gmail.com"})
      response = json_response(conn, 404)

      with %{ "errors" => %{
        "detail" => error
        }} <- response do
          assert error == "Not found"
      end
    end
  end

  defp create_test_user() do
    {:ok, user } = Account.create(%{"email_address" => @test_email})
    {:ok, %User{} = _user} = Account.update(@update_params, user)
  end
end

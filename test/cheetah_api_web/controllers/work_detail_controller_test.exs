defmodule CheetahApiWeb.WorkDetailControllerTest do
  use CheetahApiWeb.ConnCase

  import CheetahApi.Factory

  @create_attrs %{
    description: "this is a test description", start_date: "2016-04-25", end_date: "2018-04-25", location: "pune"
  }

  @invalid_attrs %{
    start_date: "2016", end_date: "2018"
  }

  @update_attrs %{
    description: "update test description", location: "jhansi"
  }

  describe "show/2" do
    test "returns work detail info if found", %{conn: conn} do
      work_detail = insert(:work_detail)
      response = 
        conn
        |> get(work_detail_path(conn, :show, work_detail.id))
        |> json_response(200)

      assert response["data"]["id"] ==  to_string(work_detail.id)
      assert response["data"]["type"] == "workDetail"
    end

    test "responds with a message indicating work_detail not found", %{conn: conn} do
      response = 
        conn
        |> get(work_detail_path(conn, :show, -1))
        |> json_response(404)

      with %{"errors" => %{"detail" => detail, "status" => status}} <- response do
        assert detail == "Not found"
        assert status == 404
      else
        _ -> assert false        
      end
    end

  end

  describe "create/2" do
    test "creates an work_detail if attributes are valid", %{conn: conn} do
      work_experience_id = insert(:work_experience).id
      professional_role_id = insert(:professional_role).id
      company_id = insert(:company).id

      attrs = Map.merge(
        @create_attrs, 
        %{work_experience_id: work_experience_id, professional_role_id: professional_role_id, company_id: company_id}
      )

      response = 
        conn
        |> post(work_detail_path(conn, :create, attrs))
        |> json_response(200)

      with %{"data" => %{"attributes" => %{"companyId" => c_id,
        "description" => description, "endDate" => end_date,
        "location" => location, "professionalRoleId" => pr_id,
        "startDate" => start_date, "workExperienceId" => we_id}}} <- response
      do
        assert description == "this is a test description"
        assert start_date == "2016-04-25"
        assert location == "pune"
        assert end_date == "2018-04-25"
        assert c_id == company_id
        assert pr_id == professional_role_id
        assert we_id == work_experience_id
      else
        _ -> assert false
      end
    end

    test "Returns an error and does not create a work_detail if attributes are invalid", %{conn: conn} do
      response = 
        conn
        |> post(work_detail_path(conn, :create, @invalid_attrs))
        |> json_response(422)

      with %{"errors" => %{"start_date" => date_error, "end_date" => date_error}} <- response do
        assert date_error == "is invalid"
      else
        _ -> assert false        
      end
    end

  end

  describe "update/2" do
    test "Edits, and responds with the work_detail if attributes are valid", %{conn: conn} do
      work_detail = insert(:work_detail)
      response = 
        conn
        |> put(work_detail_path(conn, :update, work_detail.id, @update_attrs))
        |> json_response(200)

      with %{"data" => %{"attributes" => %{
        "description" => description, 
        "location" => location}}} <- response do
        assert description == "update test description"
        assert location == "jhansi"
      else
        _ -> assert false        
      end
    end

    test "Returns an error and does not update a work_detail if attributes are invalid", %{conn: conn} do
      work_detail = insert(:work_detail)
      response = 
        conn
        |> put(work_detail_path(conn, :update, work_detail.id, @invalid_attrs))
        |> json_response(422)

      with %{"errors" => %{"start_date" => date_error, "end_date" => date_error}} <- response do
        assert date_error == "is invalid"
      else
        _ -> assert false        
      end
    end

  end

  describe "delete/2" do
    test "delete/2 and responds with :ok if the work_detail is deleted", %{conn: conn} do
      work_detail = insert(:work_detail)
      response = 
        conn
        |> delete(work_detail_path(conn, :delete, work_detail.id))
        |> json_response(200)

      assert response["data"]["id"] ==  to_string(work_detail.id)
      assert response["data"]["type"] == "workDetail"
    end

    test "responds with a message indicating work_detail not found", %{conn: conn} do
      response = 
        conn
        |> delete(work_detail_path(conn, :delete, -1))
        |> json_response(404)

      with %{"errors" => %{"detail" => detail, "status" => status}} <- response do
        assert detail == "Not found"
        assert status == 404
      else
        _ -> assert false        
      end
    end

  end

end
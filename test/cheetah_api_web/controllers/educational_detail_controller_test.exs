defmodule CheetahApiWeb.EducationalDetailControllerTest do
  use CheetahApiWeb.ConnCase

  alias CheetahApi.ProfessionalDetails

  @create_attrs %{
    institution: "ait pune", degree: "BE", start_date: "2016-04-25", location: "pune"
  }

  @update_attrs %{
    end_date: "2020-04-25", institution: "Army Institue of Technology"
  }

  @invalid_attrs %{
    start_date: "2016-25", end_date: "2018-04"
  }

  describe "show/2" do
    setup [:create_educational_detail]
    
    test "responds with educational_detail info if educational_detail is found", %{conn: conn, educational_detail: educational_detail} do
      response = 
        conn
        |> get(educational_detail_path(conn, :show, educational_detail.id))
        |> json_response(200)

      with %{"data" => %{"attributes" => %{
        "degree" => degree, "institution" => institution, 
        "location" => location, "startDate" => start_date}}} <- response
      do
        assert degree == "BE"
        assert institution == "ait pune"
        assert location == "pune"
        assert start_date == "2016-04-25"
      else
        _ -> assert false
      end

    end

    test "responds with a message indicating educational_detail not found", %{conn: conn} do
      response = 
        conn
        |> get(educational_detail_path(conn, :show, -1))
        |> json_response(404)

      with %{"errors" => %{"detail" => detail, "status" => status}} <- response do
        assert detail == "Not found"
        assert status == 404
      else
        _ -> assert false        
      end
    end

  end

  describe "create/2" do
    test "creates an educational_detail if attributes are valid", %{conn: conn} do
      response = 
        conn
        |> post(educational_detail_path(conn, :create, @create_attrs))
        |> json_response(200)

      with %{"data" => %{"attributes" => %{
        "degree" => degree, "institution" => institution, 
        "location" => location, "startDate" => start_date}}} <- response
      do
        assert degree == "BE"
        assert institution == "ait pune"
        assert location == "pune"
        assert start_date == "2016-04-25"
      else
        _ -> assert false
      end
    end

    test "Returns an error and does not create an educational_detail if attributes are invalid", %{conn: conn} do
      response = 
        conn
        |> post(educational_detail_path(conn, :create, @invalid_attrs))
        |> json_response(422)

      with %{"errors" => %{"start_date" => date_error, "end_date" => date_error}} <- response do
        assert date_error == "is invalid"
      else
        _ -> assert false        
      end
    end

  end

  describe "update/2" do
    setup [:create_educational_detail]
    
    test "Edits, and responds with the educational_detail if attributes are valid", %{conn: conn, educational_detail: educational_detail} do
      response = 
        conn
        |> put(educational_detail_path(conn, :update, educational_detail.id, @update_attrs))
        |> json_response(200)

      with %{"data" => %{"attributes" => %{
              "endDate" => end_date, "institution" => institution
            }}} <- response do
        assert end_date == "2020-04-25"
        assert institution == "Army Institue of Technology"
      else
        _ -> assert false        
      end
    end

    test "Returns an error and does not edit the educational_detail if attributes are invalid", %{conn: conn, educational_detail: educational_detail} do
      response = 
        conn
        |> put(educational_detail_path(conn, :update, educational_detail.id, @invalid_attrs))
        |> json_response(422)

      with %{"errors" => %{"start_date" => date_error, "end_date" => date_error}} <- response do
        assert date_error == "is invalid"
      else
        _ -> assert false        
      end
    end

  end

  describe "delete/2" do
    setup [:create_educational_detail]

    test "delete/2 and responds with :ok if the educational_detail is deleted", %{conn: conn, educational_detail: educational_detail} do
      response = 
        conn
        |> delete(educational_detail_path(conn, :delete, educational_detail.id))
        |> json_response(200)

      with %{"data" => %{"attributes" => %{
        "degree" => degree, "institution" => institution, 
        "location" => location, "startDate" => start_date}}} <- response
      do
        assert degree == "BE"
        assert institution == "ait pune"
        assert location == "pune"
        assert start_date == "2016-04-25"
      else
        _ -> assert false
      end
    end

    test "responds with a message indicating educational_detail not found", %{conn: conn} do
      response = 
        conn
        |> delete(educational_detail_path(conn, :delete, -1))
        |> json_response(404)

      with %{"errors" => %{"detail" => detail, "status" => status}} <- response do
        assert detail == "Not found"
        assert status == 404
      else
        _ -> assert false        
      end
    end

  end

  defp create_educational_detail(_) do
    {:ok, educational_detail} = ProfessionalDetails.create_educational_detail(@create_attrs)
    {:ok, educational_detail: educational_detail}
  end

end
defmodule WorkExperienceControllerTest do
  use CheetahApiWeb.ConnCase

  import CheetahApi.Factory

  @update_attrs %{
    total_years_experience: 7
  }

  describe "show/2" do
    test "responds with work experience info if work experience is found", %{conn: conn} do
      work_experience = insert(:work_experience)

      response = 
        conn
        |> get(work_experience_path(conn, :show, work_experience.id))
        |> json_response(200)

      assert response["data"]["id"] == to_string(work_experience.id)
      assert response["data"]["type"] == "workExperience"
    end

    test "responds with message if work experience not found", %{conn: conn} do
      response = 
        conn
        |> get(work_experience_path(conn, :show, -1))
        |> json_response(404)

      with %{"errors" => %{"detail" => detail, "status" => status}} <- response do
        assert detail == "Not found"
        assert status == 404
      else
        _ -> assert false        
      end
    end

  end

  describe "update/2" do
    test "find and update work_experience if attrs are valid", %{conn: conn} do
      work_experience = insert(:work_experience)

      response = 
        conn
        |> put(work_experience_path(conn, :update, work_experience.id, @update_attrs))
        |> json_response(200)

      with %{"data" => %{"attributes" =>
      %{"totalYearsExperience" => years}}} <- response do
        assert years == 7    
      end
    end

  end

end